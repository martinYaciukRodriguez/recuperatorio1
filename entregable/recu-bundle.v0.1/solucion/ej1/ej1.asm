global acumuladoPorCliente_asm
global en_blacklist_asm
global blacklistComercios_asm

extern malloc

%define OFFSET_MONTO 0
%define OFFSET_COMERCIO 8
%define OFFSET_CLIENTE 16
%define OFFSET_APROBADO 17

;########### SECCION DE TEXTO (PROGRAMA)
section .text

;uint32_t* acumuladoPorCliente(uint8_t cantidadDePagos, pago_t* arr_pagos)
; cantidadDePagos[rdi], arr_pagos[rsi]
acumuladoPorCliente_asm:
    push rbp
    mov rbp, rsp

    push rdi
    push rsi
    xor rax, rax
     
    mov rax, rdi
    imul rax, 8; rax = cantidadDePagos * 8, uint8_t = 1 byte + 7 (padding)
    call malloc

    pop rsi
    pop rdi

    xor rcx, rcx
    .iter:
        cmp rcx, rdi
        je .end

        xor rdx, rdx
        mov dl, byte [rsi + rcx + OFFSET_MONTO] ; monto
        xor r8, r8
        mov r8b, byte [rsi + rcx + OFFSET_APROBADO] ; aprobado
        cmp r8b, byte 0
        je .desaprobado
        mov [rax + rcx], byte dl
        jmp .idx

        .desaprobado:
            mov [rax + rcx], byte 0

        .idx:
            inc rcx
            jmp .iter

    .end:
        pop rbp
	    ret

;uint8_t en_blacklist(char* comercio, char** lista_comercios, uint8_t n)
;comercio[rdi], lista_comercios[rsi], n[rdx]
en_blacklist_asm:
    push rbp
    mov rbp, rsp

    xor rax, rax
    xor rcx, rcx
    .loop:
        cmp rcx, rdx
        je .fin

        cmp [rsi + rcx], byte dl
        je .equal
         
        inc rcx
        jmp .loop

    jmp .fin

    .equal:
        inc rax

    .fin:
        pop rbp
	    ret

;pago_t** blacklistComercios(uint8_t cantidad_pagos, pago_t* arr_pagos, char** arr_comercios, uint8_t size_comercios)
;cantidad_pagos[rdi], arr_pagos[rsi], arr_comercios[rcx], size_comercios[rdx]
blacklistComercios_asm:
    push rbp
    mov rbp, rsp

    push rdi
    push rsi

    mov rax, rdi
    imul rax, 32 ; sizeof(*arr_pagos)
    call malloc

    pop rsi
    pop rdi

    xor r8, r8
    .main:
        cmp r8, rdi
        je .fine

        xor r9, r9
        mov r9b, [rsi + r8 + OFFSET_COMERCIO]
        xor r10, r10
        .side_loop:
            cmp r10, rdx
            je .main_idx
             
            cmp r9b, byte [rcx + r10]
            je .eq

            inc r10
            jmp .side_loop
         
        .main_idx:
            inc r8
            jmp .main
         
        .eq:
            add rsi, r8
            mov [rax + r8], rsi
            jmp .main
        
    .fine:
        pop rbp
	    ret
