#include "ej1.h"

uint32_t* acumuladoPorCliente(uint8_t cantidadDePagos, pago_t* arr_pagos){
    uint32_t* res = malloc(cantidadDePagos * 8);
    for (uint8_t i=0; i < cantidadDePagos; i++) {
        if (arr_pagos[i].aprobado) {
            res[i] = arr_pagos[i].monto;
        } else {
            res[i] = 0;
        }
    }
    return res;
}

uint8_t en_blacklist(char* comercio, char** lista_comercios, uint8_t n){
    for (uint8_t i=0; i < n; i++) {
        if (lista_comercios[i] == comercio) {
            return 1;
        }
    }
    return 0;
}

pago_t** blacklistComercios(uint8_t cantidad_pagos, pago_t* arr_pagos, char** arr_comercios, uint8_t size_comercios){
    pago_t** res = malloc(cantidad_pagos * sizeof(*arr_pagos));
    for (uint8_t i=0; i < cantidad_pagos; i++) {
        if (en_blacklist(arr_pagos[i].comercio, arr_comercios, size_comercios)) {
            res[i] = &arr_pagos[i];
        }
    }
    return res;
}


