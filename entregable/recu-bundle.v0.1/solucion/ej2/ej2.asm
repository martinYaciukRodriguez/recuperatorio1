global maximosYMinimos_asm

;########### SECCION DE TEXTO (PROGRAMA)
section .text

;void maximosYMinimos_asm(uint8_t *src, uint8_t *dst, uint32_t width, uint32_t height);
;src[rdi], dst[rsi], width[rdx], height[rcx]
maximosYMinimos_asm:
    push rbp
    mov rbp, rsp

    call masks

    pxor xmm0, xmm0
    pxor xmm4, xmm4
    pxor xmm5, xmm5
    pxor xmm6, xmm6

    xor r8, r8
    .width:
        cmp r8, rdx
        je .end

        xor r9, r9
        .height:
            cmp r9, rcx
            je .width
             
            movdqu xmm0, [rdi + r8 + r9]
             
            movdqa xmm4, xmm0
            pand xmm4, xmm1 ; blue

            movdqa xmm5, xmm0
            pand xmm5, xmm2 ; green

            movdqa xmm6, xmm0
            pand xmm6, xmm3 ; red

            pxor xmm7, xmm7
            pxor xmm8, xmm8
            pxor xmm9, xmm9
             
            movdqa xmm7, xmm4
            pslldq xmm7, 1
            pcmpgtb xmm7, xmm5
            psrldq xmm7, 1
            pand xmm4, xmm7
            movdqa xmm7, xmm4
            pslldq xmm7, 2
            pcmpgtb xmm7, xmm6
            psrldq xmm7, 2
            pand xmm4, xmm7

    pop rbp
    ret

masks:
    xor r10, r10
    pxor xmm1, xmm1 ; blue
    pxor xmm2, xmm2 ; green
    pxor xmm3, xmm3 ; red

    mov r10, 0xff
    movq xmm1, r10
    punpckldq xmm1, xmm1
    punpckldq xmm1, xmm1
    punpckldq xmm1, xmm1

    movdqa xmm2, xmm1
    pslldq xmm2, 1

    movdqa xmm3, xmm2
    pslldq xmm3, 1

    ret
